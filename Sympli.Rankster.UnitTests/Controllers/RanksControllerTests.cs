﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Shouldly;
using Sympli.Rankster.App.Controllers;
using Sympli.Rankster.Contracts;
using Sympli.Rankster.Models;

namespace Sympli.Rankster.UnitTests.Controllers
{
    [TestClass]
    public class RanksControllerTests
    {
        private ISearchProviderFactory _searchProviderFactory;
        [TestInitialize]
        public void Setup()
        {
            _searchProviderFactory = Substitute.For<ISearchProviderFactory>();
            _searchProviderFactory.GetSearchProvider(Arg.Any<string>()).Returns(Substitute.For<ISearchProvider>());
        }

        [TestMethod]
        public void GetRanks_ReturnsRanksWhenRankIsFound()
        {
            var searchProvider = _searchProviderFactory.GetSearchProvider(null);
            searchProvider.GetRank(Arg.Any<SearchQuery>()).Returns(new SearchResult() {Ranks = new List<int>() {1, 7}});

            var ranksController = new RanksController(_searchProviderFactory);

            var result = ranksController.GetRank(new SearchQuery()
            {
                RankText = "www.sympli.com.au",
                SearchEngine = "Bing",
                SearchText = "e-settlements"
            });

            var jsonResult = result as JsonResult;

            jsonResult.ShouldNotBe(null);

            var searchResult = (SearchResult) jsonResult.Value;
            searchResult.ShouldNotBe(null);
            searchResult.Ranks.ShouldNotBe(null);
            searchResult.Ranks.Count.ShouldBe(2);
            searchResult.Ranks[0].ShouldBe(1);
            searchResult.Ranks[1].ShouldBe(7);
        }

        [TestMethod]
        public void GetRanks_ReturnsEmptyRanksWhenRankIsNotFound()
        {
            var searchProvider = _searchProviderFactory.GetSearchProvider(null);
            searchProvider.GetRank(Arg.Any<SearchQuery>()).Returns(new SearchResult() { Ranks = new List<int>() });

            var ranksController = new RanksController(_searchProviderFactory);

            var result = ranksController.GetRank(new SearchQuery()
            {
                RankText = "www.sympli.com.au",
                SearchEngine = "Bing",
                SearchText = "e-settlements"
            });

            var jsonResult = result as JsonResult;

            jsonResult.ShouldNotBe(null);

            var searchResult = (SearchResult)jsonResult.Value;
            searchResult.ShouldNotBe(null);
            searchResult.Ranks.ShouldNotBe(null);
            searchResult.Ranks.Count.ShouldBe(0);
        }
    }
}