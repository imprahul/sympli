﻿using System;

namespace Sympli.Rankster.Attributes
{
    public class SearchEngineAttribute : Attribute
    {
        public SearchEngineAttribute(string name)
        {
            Name = name;
        }
        public string Name { get; set; }

        public override bool Match(object obj)
        {
            var target = obj as SearchEngineAttribute;
            return Name == target?.Name;
        }
    }
}
