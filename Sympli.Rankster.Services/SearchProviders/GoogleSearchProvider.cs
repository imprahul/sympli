﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sympli.Rankster.Attributes;
using Sympli.Rankster.Contracts;
using Sympli.Rankster.Extensions;
using Sympli.Rankster.Models;

namespace Sympli.Rankster.Services.SearchProviders
{
    [SearchEngine("Google")]
    //The google search provider is implemented for the scenario where the search term is correct and is a commonly used form of the phrase.
    //For example 'e-settlements' is the common form and will work correctly, 
    //however 'e-settlement' is not common hence google says "Including results for e-settlements' which makes the ranking algorithm here a bit incorrect.
    //All such scenarios can be handled if requried, but for expediency I am skipping this for the test.
    public class GoogleSearchProvider : ISearchProvider
    {
        private readonly IUrlContentService _urlContentService;

        public GoogleSearchProvider(IUrlContentService urlContentService)
        {
            _urlContentService = urlContentService;
        }
        /// <summary>
        /// Search for the specified searchText and look for the ranks of the specified textToRank
        /// </summary>
        public SearchResult GetRank(SearchQuery searchQuery)
        {
            string searchText = searchQuery.SearchText;
            string rankText = searchQuery.RankText;
            var urlForSearch = "http://google.com/search?q=" + searchText.Trim().Replace(" ", "+") + "&num=100";
            var uri = new Uri(urlForSearch);
            var queryDiv = "<div class=\"g\"";
            var queryImages = $"Images for <b>{searchText}</b>";
            var html = _urlContentService.GetUrlContent(uri);

            var characterIndexesOfDivs = html.AllIndexesOf(queryDiv);//Regex.Matches(html, query).Cast<Match>().Select(m => m.Index).ToList();
            var characterIndexesOfUrlToCount = html.AllIndexesOf(rankText);
            var characterIndexOfImagesText = html.IndexOf(queryImages);

            var ranks = new List<int>();

            foreach (var i in characterIndexesOfUrlToCount)
            {
                var containerCharIndex = characterIndexesOfDivs.LastOrDefault(t => t < i);

                var containerIndex = characterIndexesOfDivs.IndexOf(containerCharIndex);

                if (characterIndexesOfDivs[containerIndex] < characterIndexOfImagesText && characterIndexesOfDivs[containerIndex + 1] > characterIndexOfImagesText)
                    continue;
                if (!ranks.Contains(containerIndex + 1))
                    ranks.Add(containerIndex + 1);
            }

            return new SearchResult() { Ranks = ranks };
        }
    }
}