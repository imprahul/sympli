﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Sympli.Rankster.Attributes;
using Sympli.Rankster.Contracts;
using Sympli.Rankster.Extensions;
using Sympli.Rankster.Models;

namespace Sympli.Rankster.Services.SearchProviders
{
    [SearchEngine("Bing")]
    public class BingSearchProvider : ISearchProvider
    {
        private readonly IUrlContentService _urlContentService;

        public BingSearchProvider(IUrlContentService urlContentService)
        {
            _urlContentService = urlContentService;
        }
        /// <summary>
        /// Search for the specified searchText and look for the ranks of the specified textToRank
        /// </summary>
        public SearchResult GetRank(SearchQuery searchQuery)
        {
            string searchText = searchQuery.SearchText;
            string rankText = searchQuery.RankText;
            var ranks = new List<int>();
            var startingRank = 1;
            //Bing doesn't support the concept of changing the number of records returned for each query
            //therefore we need to query bing multiple times till we reach a 100 search results.
            while (startingRank <= 100)
            {
                var urlForSearch = "https://www.bing.com/search?q=" + searchText.Trim().Replace(" ", "+") + "&first=" + startingRank;
                var uri = new Uri(urlForSearch);
                var queryDiv = "class=\"b_algo\"";
                var html = _urlContentService.GetUrlContent(uri);

                var characterIndexesOfDivs = html.AllIndexesOf(queryDiv);//Regex.Matches(html, query).Cast<Match>().Select(m => m.Index).ToList();
                var characterIndexesOfUrlToCount = html.AllIndexesOf(rankText);

                foreach (var i in characterIndexesOfUrlToCount)
                {
                    var containerCharIndex = characterIndexesOfDivs.LastOrDefault(t => t < i);

                    var containerIndex = characterIndexesOfDivs.IndexOf(containerCharIndex);

                    if (!ranks.Contains(containerIndex + startingRank) && containerIndex + startingRank <= 100)
                        ranks.Add(containerIndex + startingRank);
                }
                startingRank += characterIndexesOfDivs.Count;
            }
            return new SearchResult() {Ranks = ranks};
        }

    }
}