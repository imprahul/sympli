﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sympli.Rankster.Attributes;
using Sympli.Rankster.Contracts;

namespace Sympli.Rankster.Services.Factories
{
    public class SearchProviderFactory : ISearchProviderFactory
    {
        private readonly IEnumerable<ISearchProvider> _searchProviders;

        public SearchProviderFactory(IEnumerable<ISearchProvider> searchProviders)
        {
            _searchProviders = searchProviders;
        }

        public ISearchProvider GetSearchProvider(string searchEngineName)
        {
            return _searchProviders.FirstOrDefault(
                item =>
                    ((SearchEngineAttribute)Attribute.GetCustomAttribute(item.GetType(), typeof(SearchEngineAttribute)))
                    .Match(new SearchEngineAttribute(searchEngineName)));
        }
    }
}