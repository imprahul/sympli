﻿using System;
using System.IO;
using System.Net;
using System.Text;
using Sympli.Rankster.Contracts;

namespace Sympli.Rankster.Services
{
    public class UrlContentService : IUrlContentService
    {
        /// <summary>
        /// Reads the contents of the specified URL
        /// </summary>
        /// <param name="uri">URL to read the conent from</param>
        /// <returns>Content hosted at the specified URL.</returns>
        public string GetUrlContent(Uri uri)
        {
            var bufferForHtml = new StringBuilder();
            //8kb buffer for reading data
            var encodedBytes = new byte[8192];

            var request = WebRequest.Create(uri);
            var response = (HttpWebResponse)request.GetResponse();

            using (var responseStream = response.GetResponseStream())
            {
                if (responseStream == null)
                {
                    return string.Empty;
                }
                var enc = string.IsNullOrWhiteSpace(response.CharacterSet) ? Encoding.Default : Encoding.GetEncoding(response.CharacterSet);

                int count;
                do
                {
                    count = responseStream.Read(encodedBytes, 0, encodedBytes.Length);
                    if (count != 0)
                    {
                        var tempString = enc.GetString(encodedBytes, 0, count);
                        bufferForHtml.Append(tempString);
                    }
                }

                while (count > 0);
            }
            return bufferForHtml.ToString();
        }
    }
}