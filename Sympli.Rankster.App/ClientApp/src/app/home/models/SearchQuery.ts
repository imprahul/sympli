export class SearchQuery {
  searchText: string;
  rankText: string;
  searchEngine: string;
}
