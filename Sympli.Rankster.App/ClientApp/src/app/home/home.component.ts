import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SearchQuery } from "./models/SearchQuery";
import { SearchResult } from "./models/SearchResult";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent{
  isLoading: boolean;
  searchResult: SearchResult;
  url: string;
  httpClient: HttpClient;
  searchQuery: SearchQuery = {
    searchText: 'e-settlements',
    rankText: 'www.sympli.com.au',
    searchEngine: 'Google'
  };

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
      this.isLoading = false;
        this.httpClient = http;
        this.url = baseUrl + 'api/Ranks/GetRank';
  }

  public clearRanks() {
    this.searchResult = null;
  }

  public getRanks() {
      this.isLoading = true;
      let query = "?SearchText=" +
          this.searchQuery.searchText +
          "&RankText=" +
          this.searchQuery.rankText +
          "&SearchEngine=" +
          this.searchQuery.searchEngine;

      this.httpClient.get<SearchResult>(this.url + query).subscribe(result => {
          this.searchResult = result;
      }, error => console.error(error), () => this.isLoading = false);
  }
}
