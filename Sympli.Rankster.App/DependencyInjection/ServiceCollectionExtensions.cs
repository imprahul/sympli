﻿using Microsoft.Extensions.DependencyInjection;
using Sympli.Rankster.Contracts;
using Sympli.Rankster.Services;
using Sympli.Rankster.Services.Factories;
using Sympli.Rankster.Services.SearchProviders;

namespace Sympli.Rankster.App.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Service Collection for the Rankster application
        /// </summary>
        public static void AddRankster(this IServiceCollection services)
        {
            services.AddSingleton<IUrlContentService, UrlContentService>();
            services.AddScoped<ISearchProviderFactory, SearchProviderFactory>();
            services.AddScoped<ISearchProvider, GoogleSearchProvider>();
            services.AddScoped<ISearchProvider, BingSearchProvider>();
        }
    }
}