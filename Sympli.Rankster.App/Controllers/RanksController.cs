﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sympli.Rankster.Contracts;
using Sympli.Rankster.Models;

namespace Sympli.Rankster.App.Controllers
{
    [Route("api/[controller]")]
    public class RanksController : Controller
    {
        private readonly ISearchProviderFactory _searchProviderFactory;
        public RanksController(ISearchProviderFactory searchProviderFactory)
        {
            _searchProviderFactory = searchProviderFactory;
        }

        [HttpGet("[action]")]
        [ResponseCache(Duration = 3600, VaryByQueryKeys = new[] { "searchText", "rankText", "searchEngine" })]
        public IActionResult GetRank([FromQuery] SearchQuery searchQuery)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var searchProvider = _searchProviderFactory.GetSearchProvider(searchQuery.SearchEngine);
            return Json(searchProvider.GetRank(searchQuery));
        }
    }
}