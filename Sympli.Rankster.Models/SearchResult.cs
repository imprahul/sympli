﻿using System.Collections.Generic;

namespace Sympli.Rankster.Models
{
    public class SearchResult
    {
        public List<int> Ranks { get; set; }
    }
}