﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Sympli.Rankster.Models
{
    public class SearchQuery
    {
        [Required]
        public string SearchText { get; set; }
        [Required]
        public string RankText { get; set; }
        [Required]
        public string SearchEngine { get; set; }
    }
}
