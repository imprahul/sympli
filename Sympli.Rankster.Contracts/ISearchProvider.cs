﻿using System.Collections.Generic;
using Sympli.Rankster.Models;

namespace Sympli.Rankster.Contracts
{
    public interface ISearchProvider
    {
        /// <summary>
        /// Search for the specified searchText and look for the ranks of the specified textToRank
        /// </summary>
        SearchResult GetRank(SearchQuery searchQuery);
    }
}