﻿namespace Sympli.Rankster.Contracts
{
    public interface ISearchProviderFactory
    {
        /// <summary>
        /// Get the Search Provider for the specified search engine
        /// </summary>
        /// <param name="searchEngineName">The name of the search engine for which the provider is required</param>
        /// <returns></returns>
        ISearchProvider GetSearchProvider(string searchEngineName);
    }
}