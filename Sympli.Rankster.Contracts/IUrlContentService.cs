﻿using System;

namespace Sympli.Rankster.Contracts
{
    public interface IUrlContentService
    {
        /// <summary>
        /// Reads the contents of the specified URL
        /// </summary>
        /// <param name="uri">URL to read the conent from</param>
        /// <returns></returns>
        string GetUrlContent(Uri uri);
    }
}