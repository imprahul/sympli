﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sympli.Rankster.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// String Extension method that returns all the indexes of the specified substring in the given string
        /// </summary>
        /// <param name="sourceString">The string in which to search for the ocurrence of the specified search string.</param>
        /// <param name="searchString">The string to search for in the source string</param>
        /// <param name="ignoreCase">Ignore case when searching for the search string in the source</param>
        /// <returns>A list of indexes where the search string was found in the source string</returns>
        public static List<int> AllIndexesOf(this string sourceString, string searchString, bool ignoreCase = false)
        {
            if (string.IsNullOrWhiteSpace(sourceString) ||
                string.IsNullOrWhiteSpace(searchString))
            {
                throw new ArgumentException("String or substring is not specified.");
            }

            var indexes = new List<int>();
            var index = 0;

            while ((index = sourceString.IndexOf(searchString, index, ignoreCase ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal)) != -1)
            {
                indexes.Add(index++);
            }

            return indexes;
        }
    }
}
